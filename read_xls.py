from openpyxl import load_workbook
from pprint import pprint

file = load_workbook('archivo/Sernac_Matriz_v1.6.xlsx');

matrix_sheet = file['Matriz aspecto']

def getDocumentTypeList():
	documentTypeList = []
	for i in range(1, matrix_sheet.max_column):
		cellValue = matrix_sheet.cell(row = 1, column = i).value
		if(cellValue != None):
			documentTypeList.append(cellValue.capitalize())
	return documentTypeList;

#pprint(getDocumentTypeList())

def generateFiles(documentTypeName):
	sheet_ranges = file['Metadatos']
	merged_cell_list = sheet_ranges.merged_cell_ranges
	for i in range(len(merged_cell_list)):
		print(merged_cell_list[i])